package dev.mattia.lagreport;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin implements Listener {
	@Override
    public void onEnable() {
        System.out.println("[LagReport] Started Plugin");
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	if(label.equalsIgnoreCase("lag")) {
        	if(sender instanceof Player) {
                Player player = (Player) sender;
                if(player.hasPermission("lagreport.send")) {
                	for(Player p : this.getServer().getOnlinePlayers()) {
                		if(p.hasPermission("lagreport.receive")) {
                			if(p != player) {
                    			p.sendMessage(ChatColor.RED+"[LagReport] Reported Lag From: "+player.getDisplayName());
                			}
                		}
                		player.sendMessage(ChatColor.GREEN+"[LagReport] Lag Report Sent");
                		System.out.println("[LagReport] Reported Lag From: "+player.getDisplayName());
                		return true;
                	}
                }
        	} else {
            	for(Player p : this.getServer().getOnlinePlayers()) {
            		if(p.hasPermission("lagreport.receive")) {
            			p.sendMessage(ChatColor.RED+"[LagReport] Reported Lag From: console");
            		}
            		System.out.println("[LagReport]  Lag Report Sent");
            		return true;
            	}
        	}
        }
	    if(label.equalsIgnoreCase("lagreport")) {
	    	if(sender instanceof Player) {
	    		Player player = (Player) sender;
	    		player.sendMessage(ChatColor.GREEN+"[LagReport] v1.0 on "+this.getServer().getBukkitVersion());
	    	}
	    }
        return false;
    }
}


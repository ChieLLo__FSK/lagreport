# LagReport

LagReport is a simple light-weight plugin for your minecraft server that allows users to report lag and Administrators to receive reports

### ......................................................................................................................................................................................

## Author

**Mattia** - [ChieLLo__FSK](https://bitbucket.org/ChieLLo__FSK)


## License

This project is licensed under the MIT License - see the [LICENSE](https://bitbucket.org/MateeHash/lagreport/src/master/LICENSE) file for details.
